#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/mount.h>
#include "fluke2.h"

// defined in fluke2cmd.c
extern struct fluke_command fcmd[];
// defined in misc.c
extern struct error_record errorlog;
// defined in main.c
extern struct camera_control camctrl;
extern struct serial_control serctrl;

unsigned char scribbler_orientation;
int scribbler_echo_mode;
struct camera_window window_list[NUM_WINDOWS];
int active_window;
struct colordef blob_color;
int rle_minimum_run = 0;
unsigned char *rle_buf;
int eeprom_counter = 0;
int last_ir_power = 0;

unsigned int firmwaresize, firmwarecnt;
char *firmwarebuf = NULL;
FILE *firmwarefp = NULL;

// All functions must be non-blocking
// Non-blocking transmit functions bluetooth_send and bluetooth_putch are
// available to send a large buffer and single character, respectively.
// These functions schedule background transmissions which will take place
// at some future time when the transmit will not block.
// If additional data must be received then the function can return with
// a positive number equal to the additional data requested.  The function 
// will be re-called at a future time when that quantity of data is available.

int scribbler_passthrough(struct rfcomm_client *btclient)
{
 int fd, responselen, cmdlen, ret, i;
 unsigned char cmd, tmp;

 if(btclient->caller == CALLER_SERIAL) {
  // Scribbler2 ID string is 1 byte longer than Scribbler1
  if(serctrl.command == SCRIBCMD_GET_INFO) {
   if(serctrl.rx_cnt > 0 && serctrl.rx_buf[serctrl.rx_cnt-1] != '\n') {
    serctrl.rx_buf[serctrl.rx_cnt] = '\n';
    serctrl.rx_cnt++;
   }
   serctrl.rx_buf[serctrl.rx_cnt] = '\0';
   if(strstr(serctrl.rx_buf, "Robot:Scribbler,")) 
    serctrl.scribbler_version=1;
   else if(strstr(serctrl.rx_buf, "Robot:Scribbler2,")) 
    serctrl.scribbler_version=2;
   else serctrl.scribbler_version=0;
  }

  // Scribbler1 command echo is lost because fluke2 serial is half-duplex
  if(serctrl.scribbler_version == 1 && serctrl.command != SCRIBCMD_GET_INFO) {
   bluetooth_putstr(btclient, serctrl.tx_buf, serctrl.tx_len);
  }

  bluetooth_putstr(btclient, serctrl.rx_buf, serctrl.rx_cnt);

  return(0);
 }

 // new command

 cmd = btclient->command;
 responselen = fcmd[cmd].responselen;
 cmdlen = fcmd[cmd].cmdlen;

 if(serctrl.fd) {
  flukelog("ERROR: %s command ignored - no scribbler response to last command\n", __FUNCTION__);
  return(-1);
 }

 if(cmd == SCRIBCMD_SET_MOTORS) {
  if(scribbler_orientation) {
   // swap motors to run scribbler backwards
   tmp = btclient->rx_data[0];
   btclient->rx_data[0] = 200 - btclient->rx_data[1];
   btclient->rx_data[1] = 200 - tmp;
  }
 }

 // servo driver uses same FIQ resources as scribbler TTY
 // must close servo driver if open before using scribbler TTY

 pause_servo();

 fd = open(SCRIBBLERTTY, O_RDWR | O_NONBLOCK);
 if(fd < 0) {
  flukelog("ERROR: %s failed opening scribbler tty: %s\n", __FUNCTION__, strerror(errno));
  return(-1);
 }

 // send command packet to scribbler
 ret = write(fd, btclient->rx_buf + btclient->rx_pos, cmdlen);
 if(ret < 0) {
  flukelog("ERROR: %s failed writing to scribbler tty: %s\n", __FUNCTION__, strerror(errno));
  close(fd);
  return(-1);
 }
 else if(ret != cmdlen) {
  flukelog("ERROR: %s failed writing to scribbler tty: incomplete write\n", __FUNCTION__);
  close(fd);
  return(-1);
 }

 // Store a copy of serial transmission in case we need it later for Scrib1
 memcpy(serctrl.tx_buf, btclient->rx_buf + btclient->rx_pos, cmdlen);
 serctrl.tx_len = cmdlen;

 // setup to wait for serial response
 if(responselen > 0) {

  serctrl.fd = fd;
  serctrl.command = cmd;
  serctrl.client = btclient;
  serctrl.rx_cnt = 0;

  // Scribbler echos all transmitted chars except if the first byte of the
  //   command packet sent to the scribbler = SCRIBCMD_GET_INFO
  //   the fluke passes these echoed characters through
  // If scribbler echo mode has been set by a set_echo_mode command then
  //   the scribbler will add the command byte to the end of all packets
  //   transmitted

  if(serctrl.command == SCRIBCMD_GET_INFO || serctrl.scribbler_version == 1) 
   serctrl.rx_len = responselen;
  else serctrl.rx_len = responselen + cmdlen;
  if(scribbler_echo_mode) serctrl.rx_len++;
 }
 else {
  // done with scribbler tty
  close(fd);
  resume_servo();
 }

 if(cmd == SCRIBCMD_SET_ECHO_MODE) {
  if(btclient->rx_data[0]) scribbler_echo_mode = 1;
  else scribbler_echo_mode = 0;
 }

 if(cmd == SCRIBCMD_GET_INFO) {
  bluetooth_putstr(btclient, FLUKE2_VERSION, sizeof(FLUKE2_VERSION)-1);
  bluetooth_putch(btclient, ',');
 }

 return(0);
}

int fluke_get_errors(struct rfcomm_client *btclient)
{

 errorlog.header[0] = (errorlog.size >> 8) & 0xFF;  // high byte of 16 bits
 errorlog.header[1] = errorlog.size & 0xFF;         // low byte of 16 bits

 bluetooth_send(btclient, errorlog.header, errorlog.size + 2);

 return(0);
}

int fluke_set_picsize(struct rfcomm_client *btclient)
{
 int i;

 if(btclient->rx_data[0] == 71) {
  gpio_set_value(GPIO_PICSIZE, 1);
  camctrl.imagewidth = 427;
  camctrl.imageheight = 266;
  for(i=0;i<5;i++) if(!camera_i2c_write(0x11, 0x1)) break;
 }
 else if(btclient->rx_data[0] == 213) {
  gpio_set_value(GPIO_PICSIZE, 0);
  camctrl.imagewidth = 1280;
  camctrl.imageheight = 800;
  for(i=0;i<5;i++) if(!camera_i2c_write(0x11, 0x5)) break;
 }
 else {
  flukelog("ERROR: %s unrecognized picture size code (%d)\n", __FUNCTION__, btclient->rx_data[0]);
  return(-1);
 }

 return(0);
}

int fluke_servo(struct rfcomm_client *btclient)
{
 if(set_servo(btclient->rx_data[0], btclient->rx_data[1])) {
  flukelog("ERROR: %s set_servo failed\n", __FUNCTION__);
  return(-1);
 }

 return(0);
}

int fluke_enable_pan(struct rfcomm_client *btclient)
{
 int code;

 code = (btclient->rx_data[0] << 8) | btclient->rx_data[1];
 if(code != 0x0123) {
  flukelog("ERROR: %s invalid magic code\n", __FUNCTION__);
  return(-1);
 }

 system("/usr/bin/dbus-send --system --type=method_call --dest=org.bluez \
/org/bluez/$(pidof bluetoothd)/hci0 org.bluez.NetworkServer.Register \
string:'nap' string:'tether'");

 // Bluetooth PAN requires an authentication agent to be running
 system("/usr/local/sbin/start-stop-daemon --start --name bluetooth-agent \
--background --startas /usr/local/bin/bluetooth-agent -- 1234");

 return(0);
}

int fluke_update_firmware(struct rfcomm_client *btclient)
{
 int ret, request, code, size;

 if(btclient->caller == CALLER_MAIN_REPEAT) {
  if(!firmwarefp) {
   flukelog("ERROR: %s firmware file not open\n", __FUNCTION__);
   return(-1);
  }

  // try to use up the whole receive buffer if its only full of 
  //   firmware data to prevent it from overflowing.

  if(btclient->rx_end - btclient->rx_pos + firmwarecnt <= firmwaresize) {
   size = btclient->rx_end - btclient->rx_pos;
   btclient->rx_len = size;
  }
  else size = btclient->rx_len;

  ret = fwrite(btclient->rx_data, 1, size, firmwarefp);
  if(ret != size) {
   flukelog("ERROR: %s error writing data\n", __FUNCTION__);
   return(-1);
  }
  firmwarecnt += size;

  if(firmwarecnt >= firmwaresize) {

   fclose(firmwarefp);
   firmwarefp = NULL;

   if(chmod("/mnt/aux/fluke2_upgrade", 0755)) {
    flukelog("ERROR: %s chmod failed: %s\n", __FUNCTION__, strerror(errno));
    return(-1);
   }

   // fork off a child process to do the exec

   if(fork() == 0) {
    execl("/mnt/aux/fluke2_upgrade", NULL);
    exit(1);
   }

   // Can't umount because child process is still running
   //  Reboot will take care of the unmount

   return(0);
  }

  request = firmwaresize - firmwarecnt;
  if(request > 32768) request = 32768;

  return(request);
 }

 code = (btclient->rx_data[0] << 8) | btclient->rx_data[1];
 firmwaresize = (btclient->rx_data[2] << 24) | (btclient->rx_data[3] << 16) |
                (btclient->rx_data[4] << 8) | btclient->rx_data[5];

 if(code != 0x0123) {
  flukelog("ERROR: %s invalid magic code\n", __FUNCTION__);
  return(-1);
 }

 if(firmwaresize > 500000000) {
  flukelog("ERROR: %s invalid size\n", __FUNCTION__);
  return(-1);
 }

 if(mount("/dev/mmcblk0p2", "/mnt/aux", "ext2", 0, "")) {
  flukelog("ERROR: %s failed to mount aux filesystem: %s\n", __FUNCTION__, strerror(errno));
  return(-1);
 }

 if(firmwarefp) fclose(firmwarefp);

 firmwarefp = fopen("/mnt/aux/fluke2_upgrade", "wb");
 if(!firmwarefp) {
  flukelog("ERROR: %s failed to open upgrade file: %s\n", __FUNCTION__, strerror(errno));
  return(-1);
 }

 firmwarecnt = 0;
 request = firmwaresize;
 if(request > 32768) request = 32768;

 return(request);

}

int fluke_save_eeprom(struct rfcomm_client *btclient)
{
 FILE *fp_data;
 char buf[133];
 unsigned char cksum;
 int ret, i;

 if(eeprom_counter > 0) {
  // if computer reports a bad checksum resend last sector
  if(btclient->rx_data[0] != 42) eeprom_counter -= 132;
  else if(eeprom_counter >= 135168) {
   // done
   eeprom_counter = 0;
   return(0);
  }
 }

 if(flashdata_read(buf, eeprom_counter, 132)) {
  flukelog("ERROR: %s flashdata_read failed\n", __FUNCTION__);
  return(-1);
 }

 cksum = 0;
 for(i=0;i<132;i++) cksum += buf[i];

 bluetooth_putstr(btclient, buf, 132);
 bluetooth_putch(btclient, cksum);

 eeprom_counter += 132;

 // request another RX byte
 return(1);
}

int fluke_restore_eeprom(struct rfcomm_client *btclient)
{
 FILE *fp_data;
 unsigned char cksum;
 int ret, i;

 cksum = 0;
 for(i=0;i<132;i++) cksum += btclient->rx_data[i];
 if(cksum != btclient->rx_data[132]) {
  // checksum error - resend last segment - 1 is error code
  bluetooth_putch(btclient, 1);
  return(133);
 }

 if(flashdata_write(btclient->rx_data, eeprom_counter, 132)) {
  flukelog("ERROR: %s flashdata_write failed\n", __FUNCTION__);
  return(-1);
 }

 // 42 is success code
 bluetooth_putch(btclient, 42);

 eeprom_counter += 132;
 if(eeprom_counter >= 135168) {
  // done
  eeprom_counter = 0;
  return(0);
 }
 // request more RX bytes
 else return(133);
}

int fluke_reset(struct rfcomm_client *btclient)
{
 // not used in myro

 return(0);
}

int fluke_get_rle(struct rfcomm_client *btclient)
{
 int x, y;
 unsigned char *yuvptr;
 unsigned int rle_index, rle_count, rle_in_cnt, rle_out_cnt, rle_status;
 unsigned int size, i;

 if(btclient->caller == CALLER_CAMERA) {
  image_rgb2yuv(camctrl.image);

  // myro rle does not store rle value
  //  it assumes an initial long run of non-target color

  rle_index = 2;
  rle_count = 0;
  rle_in_cnt = 0;
  rle_out_cnt = 0;
  rle_status = 0;

  // fluke1 version only uses one pixel out of every 4
  for(i = 0; i < camctrl.imagewidth * camctrl.imageheight * 3; i+=3*4) {
   yuvptr = camctrl.image + i;
   if(yuvptr[0] >= blob_color.y_low && yuvptr[0] <= blob_color.y_high &&
      yuvptr[1] >= blob_color.u_low && yuvptr[1] <= blob_color.u_high &&
      yuvptr[2] >= blob_color.v_low && yuvptr[2] <= blob_color.v_high) {
    // pink pixel
    rle_in_cnt++;
    rle_out_cnt=0;
    if(!rle_status && rle_in_cnt >= rle_minimum_run) {
     // write out run of black
     rle_buf[rle_index++] = (rle_count >> 16) & 0xFF;
     rle_buf[rle_index++] = (rle_count >> 8) & 0xFF;
     rle_buf[rle_index++] = rle_count & 0xFF;
     rle_count = 0;
     rle_status = 1;
    }
   }
   else {
    // black pixel
    rle_in_cnt=0;
    rle_out_cnt++;
    if(rle_status && rle_out_cnt >= rle_minimum_run) {
     // write out run of pink
     rle_buf[rle_index++] = (rle_count >> 16) & 0xFF;
     rle_buf[rle_index++] = (rle_count >> 8) & 0xFF;
     rle_buf[rle_index++] = rle_count & 0xFF;
     rle_count = 0;
     rle_status = 0;
    }
   }
   rle_count++;

   if(rle_index >= RLEBUFSIZE-2) {
    flukelog("ERROR: %s out of space in rle buffer\n", __FUNCTION__);
    break;
   }
  }

  rle_buf[rle_index++] = (rle_count >> 16) & 0xFF;
  rle_buf[rle_index++] = (rle_count >> 8) & 0xFF;
  rle_buf[rle_index++] = rle_count & 0xFF;
  size = rle_index - 2;
  rle_buf[0] = (size >> 8) & 0xFF;
  rle_buf[1] = size & 0xFF;

  bluetooth_send(btclient, rle_buf, rle_index);

  return(0);
 }

 if(camera_start_grab_image()) {
  flukelog("ERROR: %s camera_start_grab_image failed\n", __FUNCTION__);
  return(-1);
 }
 camctrl.command = btclient->command;
 camctrl.client = btclient;
 camctrl.colorflag = 1;
 camctrl.dojpeg = 0;

 return(0);
}

int fluke_get_image(struct rfcomm_client *btclient)
{

 if(btclient->caller == CALLER_CAMERA) {
  image_rgb2vyuy(camctrl.image);
  bluetooth_send(btclient, camctrl.image, (camctrl.imagewidth * camctrl.imageheight));
  return(0);
 }

 if(camera_start_grab_image()) {
  flukelog("ERROR: %s camera_start_grab_image failed\n", __FUNCTION__);
  return(-1);
 }
 camctrl.command = btclient->command;
 camctrl.client = btclient;
 camctrl.colorflag = 1;

 return(0);
}

int fluke_get_image_window(struct rfcomm_client *btclient)
{
 int x, y, i;

 if(btclient->caller == CALLER_CAMERA) {
  i=0;
  for(y = window_list[active_window].ylow;
      y <= window_list[active_window].yhigh;
      y += window_list[active_window].ystep) {
   for(x = window_list[active_window].xlow;
       x <= window_list[active_window].xhigh;
       x += window_list[active_window].xstep) {
    camctrl.image[i++] = camctrl.image[ y*camctrl.imagewidth + x ];
   }
  }

  bluetooth_send(btclient, camctrl.image, i);

  return(0);
 }

 active_window = btclient->rx_data[0];
 if(active_window < 0 || active_window >= NUM_WINDOWS) active_window = 0;

 if(camera_start_grab_image()) {
  flukelog("ERROR: %s camera_start_grab_image failed\n", __FUNCTION__);
  return(-1);
 }
 camctrl.command = btclient->command;
 camctrl.client = btclient;
 camctrl.colorflag = 0;
 camctrl.dojpeg = 0;

 return(0);
}

int fluke_get_ir_left(struct rfcomm_client *btclient)
{
 // fluke2 only has a center IR emitter/sensor
 return(fluke_get_ir_center(btclient));
}

int fluke_get_ir_center(struct rfcomm_client *btclient)
{
 int i, val, cnt;

 cnt = 0;

 for(i=0;i<10;i++) {

  // with the code below GPIO_IROUT will be low for about 1-2ms
  // delay from starting pulses to sensor going low is 180us
  // delay from stopping pulses to the sensor going high is 240us

  // turn IR emitter pulses on
  gpio_set_value(GPIO_IROUT, 0);
  // check sensor
  gpio_get_value(GPIO_IRIN, &val);
  // turn IR emitter pulses off
  gpio_set_value(GPIO_IROUT, 1);

  // IR sensor is active low
  if(!val) cnt++;

  // duty cycle of output pulse train must be less than 40%
  //  total delay includes another 1-2ms from gpio calls
  udelay(1000);

  // should be no IR return now because the output pulses are off
  //   if the IR sensor is high its a false positive so decrease 
  //   the pulse counter
  gpio_get_value(GPIO_IRIN, &val);

  if(!val) cnt--;
 }

 // fluke1 value ranged from 0 to 6400
 if(cnt < 0) cnt = 0;
 cnt *= (6400/10);

 bluetooth_putch(btclient, (cnt >> 8) & 0xFF);  // high byte of 16 bit result
 bluetooth_putch(btclient, cnt & 0xFF);         // low byte of 16 bit result

 return(0);
}

int fluke_get_ir_right(struct rfcomm_client *btclient)
{
 // fluke2 only has a center IR emitter/sensor
 return(fluke_get_ir_center(btclient));
}

// getBright command in myro
int fluke_get_image_window_sum(struct rfcomm_client *btclient)
{
 int x, y;
 unsigned int sum, i;

 if(btclient->caller == CALLER_CAMERA) {
  sum=0;
  for(y = window_list[active_window].ylow,
      i = window_list[active_window].ylow * camctrl.imagewidth;
      y <= window_list[active_window].yhigh;
      y += window_list[active_window].ystep,
      i += window_list[active_window].ystep * camctrl.imagewidth) {
   for(x = window_list[active_window].xlow;
       x <= window_list[active_window].xhigh;
       x += window_list[active_window].xstep) {
    sum += camctrl.image[ i + x ];
   }
  }

  // The fluke2 image size is larger than the fluke1 so the full
  //  sum does not fit in the alloted 24 bits.  Shift to fit.
  sum >>= 4;

  bluetooth_putch(btclient, (sum >> 16) & 0xFF);  // high byte of 24 bit result
  bluetooth_putch(btclient, (sum >> 8) & 0xFF);  // high byte of 24 bit result
  bluetooth_putch(btclient, sum & 0xFF);         // low byte of 24 bit result

  return(0);
 }

 active_window = btclient->rx_data[0];
 if(active_window < 0 || active_window >= NUM_WINDOWS) active_window = 0;

 if(camera_start_grab_image()) {
  flukelog("ERROR: %s camera_start_grab_image failed\n", __FUNCTION__);
  return(-1);
 }
 camctrl.command = btclient->command;
 camctrl.client = btclient;
 camctrl.colorflag = 0;
 camctrl.dojpeg = 0;

 return(0);
}

int fluke_get_battery(struct rfcomm_client *btclient)
{
 int fd, ret, raw, final;
 float conv;

 ret = read_a2d(0, &raw, NULL);
 if(ret) {
  flukelog("ERROR: %s read_a2d failed\n", __FUNCTION__);
  return(-1);
 }

 // fluke1 battery voltage = value returned / 20.9813
 // fluke2 max value 1023 = 11.128V
 // 1023 / 20.9813 = 48.758
 // 48.758 / 11.128 = 4.382 (conversion factor from fluke2 to fluke1)

 conv = (float)raw / 4.382;
 final = (int)conv;

 bluetooth_putch(btclient, (final >> 8) & 0xFF);  // high byte of 16 bit result
 bluetooth_putch(btclient, final & 0xFF);         // low byte of 16 bit result

 return(0);
}

int fluke_get_serial_mem(struct rfcomm_client *btclient)
{
 FILE *fp_data;
 int offset, ret;
 unsigned char val;

 offset = ((btclient->rx_data[0] << 8) + btclient->rx_data[1]) * 264 +
           (btclient->rx_data[2] << 8) + btclient->rx_data[3];

 if(flashdata_read(&val, offset, 1)) {
  flukelog("ERROR: %s flashdata_read failed\n", __FUNCTION__);
  return(-1);
 }

 bluetooth_putch(btclient, val);

 return(0);
}

int fluke_get_scrib_program(struct rfcomm_client *btclient)
{
 return(0);
}

// BUG: get_camera_param doesn't work because camera_i2c_read is broken
//      this function exists in myro but I don't think its used
int fluke_get_camera_param(struct rfcomm_client *btclient)
{
 unsigned char address, data;

 address = btclient->rx_data[0];

 camera_i2c_read(address, &data);

 bluetooth_putch(btclient, data);

 return(0);
}

int fluke_get_blob_window(struct rfcomm_client *btclient)
{
 int x, y;
 unsigned char *yuvptr;
 unsigned int blob_pixels, blob_x_avg, blob_y_avg, i, j;

 if(btclient->caller == CALLER_CAMERA) {
  image_rgb2yuv(camctrl.image);

  blob_pixels = 0;
  blob_x_avg = 0;
  blob_y_avg = 0;

  for(y = window_list[active_window].ylow,
      i = window_list[active_window].ylow * camctrl.imagewidth * 3;
      y <= window_list[active_window].yhigh;
      y += window_list[active_window].ystep, 
      i += window_list[active_window].ystep * camctrl.imagewidth * 3) {

   for(x = window_list[active_window].xlow,
       j = window_list[active_window].xlow * 3;
       x <= window_list[active_window].xhigh;
       x += window_list[active_window].xstep,
       j += window_list[active_window].xstep * 3) {

    yuvptr = camctrl.image + i + j;
    if(yuvptr[0] >= blob_color.y_low && yuvptr[0] <= blob_color.y_high &&
       yuvptr[1] >= blob_color.u_low && yuvptr[1] <= blob_color.u_high &&
       yuvptr[2] >= blob_color.v_low && yuvptr[2] <= blob_color.v_high) {
     blob_pixels++;
     blob_x_avg += x;
     blob_y_avg += y;
    }
   }
  }
  if(blob_pixels > 0) {
   blob_x_avg /= blob_pixels;
   blob_y_avg /= blob_pixels;
  }

  bluetooth_putch(btclient, (blob_pixels >> 8) & 0xFF);
  bluetooth_putch(btclient, blob_pixels & 0xFF);
  bluetooth_putch(btclient, blob_x_avg & 0xFF);
  bluetooth_putch(btclient, blob_y_avg & 0xFF);

  return(0);
 }

 active_window = btclient->rx_data[0];
 if(active_window < 0 || active_window >= NUM_WINDOWS) active_window = 0;

 if(camera_start_grab_image()) {
  flukelog("ERROR: %s camera_start_grab_image failed\n", __FUNCTION__);
  return(-1);
 }
 camctrl.command = btclient->command;
 camctrl.client = btclient;
 camctrl.colorflag = 1;
 camctrl.dojpeg = 0;

 return(0);
}

int fluke_get_blob(struct rfcomm_client *btclient)
{
 int x, y;
 unsigned char *yuvptr;
 unsigned int blob_pixels, blob_x_avg, blob_y_avg, i, j;

 if(btclient->caller == CALLER_CAMERA) {
  image_rgb2yuv(camctrl.image);

  blob_pixels = 0;
  blob_x_avg = 0;
  blob_y_avg = 0;
  for(y = 0, i = 0; y < camctrl.imageheight; y++, i += camctrl.imagewidth * 3) {
   for(x = 0, j = 0; x < camctrl.imagewidth; x++, j += 3) {
    yuvptr = camctrl.image + i + j;
    if(yuvptr[0] >= blob_color.y_low && yuvptr[0] <= blob_color.y_high &&
       yuvptr[1] >= blob_color.u_low && yuvptr[1] <= blob_color.u_high &&
       yuvptr[2] >= blob_color.v_low && yuvptr[2] <= blob_color.v_high) {
     blob_pixels++;
     blob_x_avg += x;
     blob_y_avg += y;
    }
   }
  }
  if(blob_pixels > 0) {
   blob_x_avg /= blob_pixels;
   blob_y_avg /= blob_pixels;
  }

  // shift fluke2 image coordinates to fit in 1 byte
  bluetooth_putch(btclient, (blob_pixels >> 8) & 0xFF);
  bluetooth_putch(btclient, blob_pixels & 0xFF);
  bluetooth_putch(btclient, (blob_x_avg >> 3) & 0xFF);
  bluetooth_putch(btclient, (blob_y_avg >> 2) & 0xFF);

  return(0);
 }

 if(camera_start_grab_image()) {
  flukelog("ERROR: %s camera_start_grab_image failed\n", __FUNCTION__);
  return(-1);
 }
 camctrl.command = btclient->command;
 camctrl.client = btclient;
 camctrl.colorflag = 1;
 camctrl.dojpeg = 0;

 return(0);
}

int fluke_set_led_on(struct rfcomm_client *btclient)
{
 // this LED on the fluke2 is used for CPU load so use the bright LED instead
 set_pwm(BRIGHT_LED_DEFAULT_PWM);
 gpio_set_value(GPIO_BRIGHT_LED, 1);

 return(0);
}

int fluke_set_led_off(struct rfcomm_client *btclient)
{
 // this LED on the fluke2 is used for CPU load so use the bright LED instead
 gpio_set_value(GPIO_BRIGHT_LED, 0);
 // IR pwm power is shared with the bright LED
 if(last_ir_power) set_pwm(last_ir_power);

 return(0);
}

int fluke_set_rle(struct rfcomm_client *btclient)
{

 rle_minimum_run = btclient->rx_data[1];

 blob_color.y_low  = btclient->rx_data[2];
 blob_color.u_low  = btclient->rx_data[4];
 blob_color.v_low  = btclient->rx_data[6];
 blob_color.y_high = btclient->rx_data[3];
 blob_color.u_high = btclient->rx_data[5];
 blob_color.v_high = btclient->rx_data[7];

 return(0);
}

int fluke_set_ir_power(struct rfcomm_client *btclient)
{
 int pwmval;

 // IR pwm power is shared with the bright LED

 pwmval = btclient->rx_data[0] << 4;
 last_ir_power = pwmval;

 set_pwm(pwmval);

 return(0);
}

int fluke_set_serial_mem(struct rfcomm_client *btclient)
{
 FILE *fp_data;
 int offset, ret;
 unsigned char val;

 offset = ((btclient->rx_data[0] << 8) + btclient->rx_data[1]) * 264 +
           (btclient->rx_data[2] << 8) + btclient->rx_data[3];

 val = btclient->rx_data[4];

 if(flashdata_write(&val, offset, 1)) {
  flukelog("ERROR: %s flashdata_write failed\n", __FUNCTION__);
  return(-1);
 }

 return(0);
}

int fluke_set_scrib_program(struct rfcomm_client *btclient)
{
 return(0);
}

int fluke_set_scrib2_program(struct rfcomm_client *btclient)
{
 int code, size, version;

 code = (btclient->rx_data[0] << 8) | btclient->rx_data[1];
 size = (btclient->rx_data[2] << 8) | btclient->rx_data[3];

 if(!firmwarebuf) {
  flukelog("ERROR: %s missing firmware data\n", __FUNCTION__);
  return(-1);
 }

 if(code != 0x0123) {
  flukelog("ERROR: %s invalid magic code\n", __FUNCTION__);
  return(-1);
 }

 if(size != firmwaresize) {
  flukelog("ERROR: %s firmware size mismatch\n", __FUNCTION__);
  return(-1);
 }

 if(serctrl.fd) {
  flukelog("ERROR: %s serial port busy\n", __FUNCTION__);
  return(-1);
 }

 set_pwm(BRIGHT_LED_DEFAULT_PWM);

 pause_servo();

 // Fork off another process to prevent blocking

 if(fork() == 0) {

  if(prop_serial_init(SCRIBBLERTTY)) {
   flukelog("ERROR: %s failed opening serial port\n", __FUNCTION__);
   return(-1);
  }

  prop_hwreset();

  if(prop_hwfind(&version)) {
   flukelog("ERROR: %s scribbler2 communication failure\n", __FUNCTION__);
   prop_serial_done();
   return(-1);
  }

  if(prop_upload(firmwarebuf, size, PROP_DOWNLOAD_RUN_EEPROM)) {
   flukelog("ERROR: %s scribbler2 programming failure\n", __FUNCTION__);
   prop_serial_done();
   return(-1);
  }

  prop_serial_done();

  // doesn't work because when child exits servo fd's will be closed
  resume_servo();

  exit(0);
 }

 // OK to free firmwarebuf even though it is currently in use by child
 //   because child has a copy of all malloc'd space

 free(firmwarebuf);
 firmwarebuf = NULL;

 return(0);

}

int fluke_reset_scrib2(struct rfcomm_client *btclient)
{
 prop_hwreset();

 return(0);
}

int fluke_set_scrib_program_batch(struct rfcomm_client *btclient)
{

 if(btclient->caller == CALLER_MAIN_REPEAT) {
  if(!firmwarebuf) {
   flukelog("ERROR: %s null firmware buffer\n", __FUNCTION__);
   return(-1);
  }
  memcpy(firmwarebuf, btclient->rx_data, firmwaresize);
  return(0);
 }

 firmwaresize = (btclient->rx_data[0] << 8) | btclient->rx_data[1];

 if(firmwarebuf) free(firmwarebuf);

 firmwarebuf = (char *)malloc(firmwaresize+100);
 if(!firmwarebuf) {
  flukelog("ERROR: %s firmwarebuf malloc failed %d bytes\n", __FUNCTION__, firmwaresize);
  return(-1);
 }

 return(firmwaresize);
}

int fluke_identify_robot(struct rfcomm_client *btclient)
{
 int version;
 char s2str[] = "SCRIBBLER-2\n";
 char s1str[] = "SCRIBBLER-1\n";
 char unknownstr[] = "UNKNOWN\n";
 char *robotstr;

 robotstr = unknownstr;

 if(serctrl.fd) {
  flukelog("ERROR: %s serial port busy\n", __FUNCTION__);
  return(-1);
 }

 pause_servo();

 if(prop_serial_init(SCRIBBLERTTY)) {
  flukelog("ERROR: %s failed opening serial port\n", __FUNCTION__);
  return(-1);
 }

 prop_hwreset();

 if(!prop_hwfind(&version)) {
  robotstr = s2str;
 }
 else {

  // BUG: no test for scribbler1 ...

 }

 prop_serial_done();

 resume_servo();

 bluetooth_putstr(btclient, robotstr, strlen(robotstr));

 return(0);
}

int fluke_set_start_program(struct rfcomm_client *btclient)
{
 return(0);
}

int fluke_reset_scribbler(struct rfcomm_client *btclient)
{
 prop_hwreset();

 return(0);
}

int fluke_serial_erase(struct rfcomm_client *btclient)
{
 FILE *fp_data;
 int offset, ret;
 char buf[266];

 offset = ((btclient->rx_data[0] << 8) + btclient->rx_data[1]) * 264;
 memset(buf, 255, 264);

 if(flashdata_write(buf, offset, 264)) {
  flukelog("ERROR: %s flashdata_write failed\n", __FUNCTION__);
  return(-1);
 }

 return(0);
}

int fluke_set_bright_led(struct rfcomm_client *btclient)
{
 int pwmval;

 if(btclient->rx_data[0] != 0) {
  pwmval = btclient->rx_data[0] << 4;
  set_pwm(pwmval);
  gpio_set_value(GPIO_BRIGHT_LED, 1);
 }
 else {
  gpio_set_value(GPIO_BRIGHT_LED, 0);
  // IR pwm power is shared with the bright LED
  if(last_ir_power) set_pwm(last_ir_power);
 }

 return(0);
}

int fluke_set_window(struct rfcomm_client *btclient)
{
 int window;

 window = btclient->rx_data[0];
 if(window < 0 || window >= NUM_WINDOWS) window = 0;

 window_list[window].xlow = 
    (btclient->rx_data[1] << 8) | btclient->rx_data[2];
 if(window_list[window].xlow > camctrl.imagewidth - 1)
    window_list[window].xlow = camctrl.imagewidth - 1;
 if(window_list[window].xlow < 0) window_list[window].xlow = 0;

 window_list[window].yhigh = 
    camctrl.imageheight - 1 - ( (btclient->rx_data[3] << 8) | btclient->rx_data[4] );
 if(window_list[window].yhigh > camctrl.imageheight - 1) 
    window_list[window].yhigh = camctrl.imageheight - 1;
 if(window_list[window].yhigh < 0) window_list[window].yhigh = 0;

 window_list[window].xhigh = 
    (btclient->rx_data[5] << 8) | btclient->rx_data[6];
 if(window_list[window].xhigh > camctrl.imagewidth - 1)
    window_list[window].xhigh = camctrl.imagewidth - 1;
 if(window_list[window].xhigh < 0) window_list[window].xhigh = 0;

 window_list[window].ylow = 
    camctrl.imageheight - 1 - ( (btclient->rx_data[7] << 8) | btclient->rx_data[8] );
 if(window_list[window].ylow > camctrl.imageheight - 1)
    window_list[window].ylow = camctrl.imageheight - 1;
 if(window_list[window].ylow < 0) window_list[window].ylow = 0;

 window_list[window].xstep = btclient->rx_data[9];
 if(window_list[window].xstep < 1) window_list[window].xstep = 1;

 window_list[window].ystep = btclient->rx_data[10];
 if(window_list[window].ystep < 1) window_list[window].ystep = 1;

 return(0);
}

int fluke_set_forwardness(struct rfcomm_client *btclient)
{
 scribbler_orientation = btclient->rx_data[0];

 if(scribbler_orientation == 0) {
  scribbler_orientation = 0xDF;
  flashdata_write(&scribbler_orientation, 0, 1);
  scribbler_orientation = 0;
 }
 else {
  scribbler_orientation = 1;
  flashdata_write(&scribbler_orientation, 0, 1);
 }

 return(0);
}

int fluke_white_balance_on(struct rfcomm_client *btclient)
{
 // restore defaults - enable automatic white balance correction
 camera_i2c_write(0x38, 0x10);
 camera_i2c_write(0x96, 0xF9);

 return(0);
}

int fluke_white_balance_off(struct rfcomm_client *btclient)
{
 // disable automatic white balance correction
 camera_i2c_write(0x38, 0x00);
 camera_i2c_write(0x96, 0xE9);

 return(0);
}

int fluke_set_camera_param(struct rfcomm_client *btclient)
{
 unsigned char address, data;

 address = btclient->rx_data[0];
 data = btclient->rx_data[1];

 // to allow backward compatiblity intercept the commonly used 
 //  address/data combos used with the fluke1
 // fluke2 camera can set the combined auto exposure/gain target so there 
 //  is no need to disable agc/aec or program the exposure/gain individually.

 if(address == 0x12 && data == 0x14) {
  // enable automatic white balance correction
  // camera_i2c_write(0x38, 0x10);
  // camera_i2c_write(0x96, 0xF9);
 }
 else if(address == 0x12 && data == 0x10) {
  // disable automatic white balance correction
  // camera_i2c_write(0x38, 0x00);
  // camera_i2c_write(0x96, 0xE9);
 }
 else if(address == 0x13 && data == 0xA3) {
  // enable auto exposure and gain control
  // camera_i2c_write(0x13, 0x85);
 }
 else if(address == 0x13 && data == 0xA0) {
  // disable auto exposure and gain control
  // camera_i2c_write(0x13, 0x00);
 }
 else if(address == 0x01) {
  // auto white balance blue channel gain - fluke1 default is 0x80
 }
 else if(address == 0x02) {
  // auto white balance red channel gain - fluke1 default is 0x80
 }
 else if(address == 0x00) {
  // set manual gain level - fluke1 default is 0x00
  camera_set_gain(data-128);
 }
 else if(address == 0x06) {
  // set manual gain level - fluke1 default is 0x80
  // camera_set_gain((int)data - 0x80);
 }
 else if(address == 0x10) {
  // set manual exposure level - fluke1 default is 0x41
  // disable to allow myro darkenCamera to work
  // camera_set_gain((int)data - 0x41);
 }
 else camera_i2c_write(address, data);

 return(0);
}

int fluke_set_uart(struct rfcomm_client *btclient)
{
 // serial FIQ driver does not currently support changing baud rate 
 //  baud rate is hard coded to 38400
 return(0);
}

int fluke_pass_byte(struct rfcomm_client *btclient)
{
 // not used in myro
 return(0);
}

int fluke_set_passthrough(struct rfcomm_client *btclient)
{
 // not used in myro
 return(0);
}

int fluke_get_jpeg_grey_header(struct rfcomm_client *btclient)
{
 int headersize;

 if(btclient->caller == CALLER_CAMERA) {
  headersize = 328;   // from fluke1 firmware
  camctrl.jpegmyro[0] = headersize & 0xFF;
  camctrl.jpegmyro[1] = (headersize >> 8) & 0xFF;

  bluetooth_send(btclient, camctrl.jpegmyro, headersize + 2);

  return(0);
 }

 if(camera_start_grab_image()) {
  flukelog("ERROR: %s camera_start_grab_image failed\n", __FUNCTION__);
  return(-1);
 }
 camctrl.command = btclient->command;
 camctrl.client = btclient;
 camctrl.colorflag = 0;

 return(0);
}

int fluke_get_jpeg_grey_scan(struct rfcomm_client *btclient)
{
 int headersize;

 if(btclient->caller == CALLER_CAMERA || (camctrl.jpegsize > 0 &&
    btclient->lastcommand == FLUKECMD_GET_JPEG_GREY_HEADER)) {
  headersize = 328;   // from fluke1 firmware
  // + 12 is for timer data
  // BUG: timerdata is currently random unitialized
  bluetooth_send(btclient, camctrl.jpeg + headersize,
                 camctrl.jpegsize - headersize + 12);

  return(0);
 }

 if(camera_start_grab_image()) {
  flukelog("ERROR: %s camera_start_grab_image failed\n", __FUNCTION__);
  return(-1);
 }
 camctrl.command = btclient->command;
 camctrl.client = btclient;
 camctrl.colorflag = 0;

 // BUG: image already captured in get_jpeg_header function 
 // jpeg fast flag
 if(!btclient->rx_data[0]) camctrl.jpegquality = JPEG_QUALITY_BAD;

 return(0);
}

int fluke_get_jpeg_color_header(struct rfcomm_client *btclient)
{
 int headersize;

 if(btclient->caller == CALLER_CAMERA) {
  headersize = 623;   // from fluke1 firmware
  camctrl.jpegmyro[0] = headersize & 0xFF;
  camctrl.jpegmyro[1] = (headersize >> 8) & 0xFF;

  bluetooth_send(btclient, camctrl.jpegmyro, headersize + 2);

  return(0);
 }

 if(camera_start_grab_image()) {
  flukelog("ERROR: %s camera_start_grab_image failed\n", __FUNCTION__);
  return(-1);
 }
 camctrl.command = btclient->command;
 camctrl.client = btclient;
 camctrl.colorflag = 1;

 return(0);
}

int fluke_get_jpeg_color_scan(struct rfcomm_client *btclient)
{
 int headersize;

 if(btclient->caller == CALLER_CAMERA || (camctrl.jpegsize > 0 && 
    btclient->lastcommand == FLUKECMD_GET_JPEG_COLOR_HEADER)) {
  headersize = 623;   // from fluke1 firmware
  // + 12 is for timer data
  // BUG: timerdata is currently random unitialized
  bluetooth_send(btclient, camctrl.jpeg + headersize,
                 camctrl.jpegsize - headersize + 12);

  return(0);
 }

 if(camera_start_grab_image()) {
  flukelog("ERROR: %s camera_start_grab_image failed\n", __FUNCTION__);
  return(-1);
 }
 camctrl.command = btclient->command;
 camctrl.client = btclient;
 camctrl.colorflag = 1;

 // BUG: image already captured in get_jpeg_header function
 // jpeg fast flag
 if(!btclient->rx_data[0]) camctrl.jpegquality = JPEG_QUALITY_BAD;

 return(0);
}

int fluke_set_pass_n_bytes(struct rfcomm_client *btclient)
{
 // not used in myro
 return(0);
}

int fluke_get_pass_n_bytes(struct rfcomm_client *btclient)
{
 // not used in myro
 return(0);
}

int fluke_get_pass_bytes_until(struct rfcomm_client *btclient)
{
 // not used in myro
 return(0);
}

int fluke_get_version(struct rfcomm_client *btclient)
{
 bluetooth_putstr(btclient, FLUKE2_VERSION, strlen(FLUKE2_VERSION));
 bluetooth_putch(btclient, '\n');

 return(0);
}

int fluke_set_passthrough_on(struct rfcomm_client *btclient)
{
 // not used in myro
 return(0);
}

int fluke_set_passthrough_off(struct rfcomm_client *btclient)
{
 // not used in myro
 return(0);
}

int fluke_get_ir_message(struct rfcomm_client *btclient)
{
 // BUG: get_ir_message not implemented but exists in myro
 return(0);
}

int fluke_send_ir_message(struct rfcomm_client *btclient)
{
 // BUG: send_ir_message not implemented but exists in myro
 return(0);
}

int fluke_set_ir_emitters(struct rfcomm_client *btclient)
{
 // BUG: set_ir_emitters not implemented but exists in myro
 return(0);
}
