/**
 * based on PLoadLib.c from the propgcc project
 *
 * Copyright (c) 2009 by John Steven Denson
 * Modified in 2011 by David Michael Betz
 *
 * MIT License                                                           
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include "fluke2.h"

#define SERFIQ_IOCTL_SCRIB2_GETBIT   37

static int serial_fd = -1;

int prop_serial_init(const char* port)
{
 serial_fd = open(port, O_RDWR | O_NONBLOCK);
 if(serial_fd < 0) {
  flukelog("ERROR: %s failed opening serial port: %s\n", __FUNCTION__, strerror(errno));
  return(-1);
 }

 return(0);
}

void prop_serial_done(void)
{
 if (serial_fd >= 0) {
  close(serial_fd);
  serial_fd = -1;
 }
}

void prop_hwreset(void)
{
 gpio_set_value(GPIO_SCRIB_RST, 0);
 msleep(10);
 gpio_set_value(GPIO_SCRIB_RST, 1);
 msleep(90);
}

int prop_lfsr_iterate(uint8_t *lfsr)
{
 int bit = (*lfsr) & 1;
 (*lfsr) = (uint8_t)(((*lfsr) << 1) | ((((*lfsr) >> 7) ^ ((*lfsr) >> 5) ^ ((*lfsr) >> 4) ^ ((*lfsr) >> 1)) & 1));

 return(bit);
}

// Fluke2 prop_getBit has no concept of a RX timeout because the serial port
//   is only half duplex.

int prop_getBit(unsigned char *data)
{
 if(!data) return(-1);

 if(ioctl(serial_fd, SERFIQ_IOCTL_SCRIB2_GETBIT, data)) {
  flukelog("ERROR: %s error on serial ioctl: %s\n", __FUNCTION__, strerror(errno));
  return(-1);
 }

 return(0);
}

int prop_sendlong(uint32_t data)
{
 uint8_t buf[12];
 int ret, n;

 for(n=0; n < 10; n++) {
     buf[n] = (uint8_t)(0x92 | (data & 1) | ((data & 2) << 2) | ((data & 4) << 4));
     data >>= 3;
 }
 buf[n] = (0xf2 | (data & 1) | ((data & 2) << 2));

 ret = write(serial_fd, buf, 11);
 if(ret != 11) {
  flukelog("ERROR: %s failed writing to serial port: %s\n", __FUNCTION__, strerror(errno));
  return(-1);
 }

 return(ret);
}

int prop_hwfind(int *version)
{
 uint8_t lfsr;
 uint8_t mybuf[300];
 int ret, ver, n;
 unsigned char data;

 // Do not pause after reset.
 // Propeller can give up if it does not see a response in 100ms of reset.

 mybuf[0] = 0xF9;
 ret = write(serial_fd, mybuf, 1);
 if(ret != 1) {
  flukelog("ERROR: %s failed writing to serial port: %s\n", __FUNCTION__, strerror(errno));
  return(-1);
 }

 // Send the magic propeller LFSR byte stream.
 lfsr = 'P';  // P is for Propeller :)
 for(n = 0; n < 250; n++) mybuf[n] = prop_lfsr_iterate(&lfsr) | 0xfe;
 ret = write(serial_fd, mybuf, 250);
 if(ret != 250) {
  flukelog("ERROR: %s failed writing to serial port: %s\n", __FUNCTION__, strerror(errno));
  return(-1);
 }

 for(n=0; n < 250; n++) {
  if(prop_getBit(&data)) {
   flukelog("ERROR: %s prop_getBit failed\n", __FUNCTION__);
   return(-1);
  }

  if(data != prop_lfsr_iterate(&lfsr)) {
   flukelog("ERROR: %s LFSR mismatch - communication error\n", __FUNCTION__);
   return(-1);
  }
 }

 // Propeller Version
 ver = 0;
 for(n=0; n < 8; n++) {
  ver >>= 1;

  if(prop_getBit(&data)) {
   flukelog("ERROR: %s prop_getBit failed\n", __FUNCTION__);
   return(-1);
  }

  ver |= data;
 }

 if(version) *version = ver;

 return(0);
}

int prop_upload(const uint8_t* dlbuf, int count, int type)
{
 int remaining, toggle, n;
 uint32_t data;
 int longcount = count/4;
 time_t timeout;
 unsigned char ack;

 // send type
 if(prop_sendlong(type) < 0) {
  flukelog("ERROR: %s prop_sendlong type failed\n", __FUNCTION__);
  return(-1);
 }

 // send count
 if(prop_sendlong(longcount) < 0) {
  flukelog("ERROR: %s prop_sendlong count failed\n", __FUNCTION__);
  return(-1);
 }

 if((type == 0) || (type >= PROP_DOWNLOAD_SHUTDOWN)) {
  // flukelog("Shutdown type %d sent\n", type);
  return(0);
 }

/*
 flukelog("Loading");
 if(type == PROP_DOWNLOAD_RUN_BINARY) flukelog(" to hub memory\n");
 else flukelog(" to EEPROM via hub memory\n");
*/

 toggle = 0;
 remaining = count;
 for(n=0; n < count; n+=4) {
  if(n % 1024 == 0) {
   if(toggle) gpio_set_value(GPIO_BRIGHT_LED, 1);
   else gpio_set_value(GPIO_BRIGHT_LED, 0);
   toggle ^= 0xFF;
   // flukelog("%d bytes remaining             \r", remaining);
   // fflush(stdout);
   // remaining -= 1024;
  }
  data = dlbuf[n] | (dlbuf[n+1] << 8) | (dlbuf[n+2] << 16) | (dlbuf[n+3] << 24) ;
  if(prop_sendlong(data) < 0) {
   flukelog("ERROR: %s prop_sendlong data failed\n", __FUNCTION__);
   return(-1);
  }
 }

/*
 flukelog("                               \r");
 flukelog("%d bytes sent\n", count);
 fflush(stdout);
*/

 msleep(50);                 // give propeller time to calculate checksum match 32K/12M sec = 32ms

/*
 // Check for a RAM program complete signal
 flukelog("Verifying RAM ... ");
 fflush(stdout);
*/

 timeout = time(NULL) + 4;
 do {
  msleep(20);
  if(prop_getBit(&ack)) {
   flukelog("ERROR: %s prop_getBit failed on RAM verify\n", __FUNCTION__);
   return(-1);
  }
  if(!ack) break;
 } while(time(NULL) <= timeout);

 if(time(NULL) > timeout) {
  flukelog("RAM verify checksum error\n");
  return(-1);
 }

 // flukelog("OK\n");

 /* If type is DOWNLOAD_EEPROM or DOWNLOAD_RUN_EEPROM
  * Check for a complete signal, then check for verify signal
  * Otherwise the board will shutdown in an undesirable way.
  */
 if(type & PROP_DOWNLOAD_EEPROM) {
/*
  flukelog("Programming EEPROM ... ");
  fflush(stdout);
*/

  timeout = time(NULL) + 30;
  do {
   msleep(40);
   if(prop_getBit(&ack)) {
    flukelog("ERROR: %s prop_getBit failed on EEPROM program\n", __FUNCTION__);
    return(-1);
   }
   if(!ack) break;
  } while(time(NULL) <= timeout);

  if(time(NULL) > timeout) {
   flukelog("EEPROM programming error\n");
   return(-1);
  }

/*
  flukelog("OK\n");

  flukelog("Verifying EEPROM ... ");
  fflush(stdout);
*/

  timeout = time(NULL) + 30;
  do {
   msleep(40);
   if(prop_getBit(&ack)) {
    flukelog("ERROR: %s prop_getBit failed on EEPROM verify\n", __FUNCTION__);
    return(-1);
   }
   if(!ack) break;
  } while(time(NULL) <= timeout);

  if(time(NULL) > timeout) {
   flukelog("EEPROM verify error\n");
   return(-1);
  }

  // flukelog("OK\n");
 }

 return(0);
}
