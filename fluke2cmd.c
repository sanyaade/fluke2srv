#include <stdio.h>
#include "fluke2.h"

// Fluke command array
//
// First struct element is the command byte code
// Second element is the packet length for this command.  The main loop
//  will only call the function when this number of bytes have been received.
// Third element is the expected amount of data to receive from the scribbler
//  for this command
// Fourth element is the function pointer to be called for this command.
// 
// The main loop uses the first byte received as an index into this array
//
// the scribbler echos back all commands so responselen includes cmdlen
//  except for get_info command which is not echoed

struct fluke_command fcmd[] = {
 {   0,  1,  0, NULL },
 {   1,  1,  0, NULL },
 {   2,  1,  0, NULL },
 {   3,  1,  0, NULL },
 {   4,  1,  0, NULL },
 {   5,  1,  0, NULL },
 {   6,  1,  0, NULL },
 {   7,  1,  0, NULL },
 {   8,  1,  0, NULL },
 {   9,  1,  0, NULL },
 {  10,  1,  0, fluke_get_errors },    // read error log - new for fluke2
 {  11,  2,  0, fluke_set_picsize },
 {  12,  3,  0, fluke_servo },
 {  13,  3,  0, fluke_enable_pan },    // enable bluetooth PAN networking
 {  14,  1,  0, NULL },
 {  15,  1,  0, NULL },
 {  16,  1,  0, NULL },
 {  17,  1,  0, NULL },
 {  18,  1,  0, NULL },
 {  19,  1,  0, NULL },
 {  20,  1,  0, NULL },
 {  21,  1,  0, NULL },
 {  22,  1,  0, NULL },
 {  23,  1,  0, NULL },
 {  24,  1,  0, NULL },
 {  25,  1,  0, NULL },
 {  26,  1,  0, NULL },
 {  27,  1,  0, NULL },
 {  28,  1,  0, NULL },
 {  29,  1,  0, NULL },
 {  30,  1,  0, NULL },
 {  31,  1,  0, NULL },
 {  32,  1,  0, NULL },
 {  33,  9,  0, scribbler_passthrough },   // soft reset
 {  34,  1,  0, NULL },
 {  35,  1,  0, NULL },
 {  36,  1,  0, NULL },
 {  37,  1,  0, NULL },
 {  38,  1,  0, NULL },
 {  39,  1,  0, NULL },
 {  40,  7,  0, fluke_update_firmware },
 {  41,  1,  0, fluke_save_eeprom },
 {  42,  134, 0, fluke_restore_eeprom },
 {  43,  1,  0, fluke_reset },
 {  44,  1,  0, NULL },
 {  45,  1,  0, NULL },
 {  46,  1,  0, NULL },
 {  47,  1,  0, NULL },
 {  48,  1,  0, NULL },
 {  49,  1,  0, NULL },
 {  50,  9,  8, scribbler_passthrough },   // get pass1
 {  51,  9,  8, scribbler_passthrough },   // get pass2
 {  52,  1,  0, NULL },
 {  53,  1,  0, NULL },
 {  54,  1,  0, NULL },
 {  55,  9, 11, scribbler_passthrough },   // set pass1
 {  56,  9, 11, scribbler_passthrough },   // set pass2
 {  57,  1,  0, NULL },
 {  58,  1,  0, NULL },
 {  59,  1,  0, NULL },
 {  60,  1,  0, NULL },
 {  61,  1,  0, NULL },
 {  62,  1,  0, NULL },
 {  63,  1,  0, NULL },
 {  64,  9,  8, scribbler_passthrough },   // get name2
 {  65,  9, 11, scribbler_passthrough },   // get all
 {  66,  9,  1, scribbler_passthrough },   // get all binary
 {  67,  9,  2, scribbler_passthrough },   // get light left
 {  68,  9,  2, scribbler_passthrough },   // get light center
 {  69,  9,  2, scribbler_passthrough },   // get light right
 {  70,  9,  6, scribbler_passthrough },   // get light all
 {  71,  9,  1, scribbler_passthrough },   // get ir left
 {  72,  9,  1, scribbler_passthrough },   // get ir right
 {  73,  9,  2, scribbler_passthrough },   // get ir all
 {  74,  9,  1, scribbler_passthrough },   // get line left
 {  75,  9,  1, scribbler_passthrough },   // get line right
 {  76,  9,  2, scribbler_passthrough },   // get line all
 {  77,  9,  2, scribbler_passthrough },   // get state
 {  78,  9,  8, scribbler_passthrough },   // get name1
 {  79,  9,  1, scribbler_passthrough },   // get stall
 {  SCRIBCMD_GET_INFO,  9, 48, scribbler_passthrough },   // get info RESPONSELEN is 48 with Scribbler1 and 49 with Scribbler2 - NO ECHO
 {  81,  9,  8, scribbler_passthrough },   // get data
 {  82,  1,  0, fluke_get_rle },
 {  83,  1,  0, fluke_get_image },
 {  84,  2,  0, fluke_get_image_window },
 {  85,  1,  0, fluke_get_ir_left },
 {  86,  1,  0, fluke_get_ir_center },
 {  87,  1,  0, fluke_get_ir_right },
 {  88,  2,  0, fluke_get_image_window_sum },
 {  89,  1,  0, fluke_get_battery },
 {  90,  5,  0, fluke_get_serial_mem },
 {  91,  2,  0, fluke_get_scrib_program },
 {  92,  2,  0, fluke_get_camera_param },
 {  93,  1,  0, NULL },            // fluke1 does not used this CMD
 {  94,  2,  0, fluke_get_blob_window },
 {  95,  1,  0, fluke_get_blob },
 {  96,  9, 11, scribbler_passthrough },   // set single data
 {  97,  9, 11, scribbler_passthrough },   // set data
 {  SCRIBCMD_SET_ECHO_MODE,  9, 11, scribbler_passthrough },   // set echo mode
 {  99,  9, 11, scribbler_passthrough },   // set led left on
 { 100,  9, 11, scribbler_passthrough },   // set led left off
 { 101,  9, 11, scribbler_passthrough },   // set led center on
 { 102,  9, 11, scribbler_passthrough },   // set led center off
 { 103,  9, 11, scribbler_passthrough },   // set led right on
 { 104,  9, 11, scribbler_passthrough },   // set led right off
 { 105,  9, 11, scribbler_passthrough },   // set led all on
 { 106,  9, 11, scribbler_passthrough },   // set led all off
 { 107,  9, 11, scribbler_passthrough },   // set led all
 { 108,  9, 11, scribbler_passthrough },   // set motors off
 { SCRIBCMD_SET_MOTORS,  9, 11, scribbler_passthrough },   // set motors
 { 110,  9, 11, scribbler_passthrough },   // set name1
 { 111,  9, 11, scribbler_passthrough },   // set loud
 { 112,  9, 11, scribbler_passthrough },   // set quiet
 { 113,  9, 11, scribbler_passthrough },   // set speaker
 { 114,  9, 11, scribbler_passthrough },   // set speaker2
 { 115,  1,  0, NULL },
 { 116,  1,  0, fluke_set_led_on },
 { 117,  1,  0, fluke_set_led_off },
 { 118,  9,  0, fluke_set_rle },
 { 119,  9, 11, scribbler_passthrough },   // set name2
 { 120,  2,  0, fluke_set_ir_power },            // set IR power
 { 121,  6,  0, fluke_set_serial_mem },
 { 122,  0,  0, fluke_set_scrib_program },   // FIX CMDLEN PARAM
 { 123,  4,  0, fluke_set_start_program },   // FIX CMDLEN PARAM
 { 124,  1,  0, fluke_reset_scribbler },
 { 125,  3,  0, fluke_serial_erase },
 { 126,  2,  0, fluke_set_bright_led },
 { 127, 12,  0, fluke_set_window },
 { 128,  2,  0, fluke_set_forwardness },
 { 129,  1,  0, fluke_white_balance_on },
 { 130,  1,  0, fluke_white_balance_off },
 { 131,  3,  0, fluke_set_camera_param },
 { 132,  4,  0, fluke_set_uart },
 { 133,  2,  0, fluke_pass_byte },
 { 134,  2,  0, fluke_set_passthrough },
 { FLUKECMD_GET_JPEG_GREY_HEADER,  1,  0, fluke_get_jpeg_grey_header },
 { 136,  2,  0, fluke_get_jpeg_grey_scan },
 { FLUKECMD_GET_JPEG_COLOR_HEADER,  1,  0, fluke_get_jpeg_color_header },
 { 138,  2,  0, fluke_get_jpeg_color_scan },
 { 139,  2,  0, fluke_set_pass_n_bytes },
 { 140,  2,  0, fluke_get_pass_n_bytes },
 { 141,  2,  0, fluke_get_pass_bytes_until },
 { 142,  1,  0, fluke_get_version },
 { 143,  1,  0, fluke_set_passthrough_on },
 { 144,  1,  0, fluke_set_passthrough_off },
 { 145,  1,  0, NULL },
 { 146,  1,  0, NULL },
 { 147,  1,  0, NULL },
 { 148,  1,  0, NULL },
 { 149,  1,  0, NULL },
 { 150,  1,  0, fluke_get_ir_message },
 { 151,  2,  0, fluke_send_ir_message },
 { 152,  2,  0, fluke_set_ir_emitters },
 { 153,  5,  0, fluke_set_scrib2_program },
 { 154,  1,  0, fluke_reset_scrib2 },
 { 155,  3,  0, fluke_set_scrib_program_batch },
 { 156,  1,  0, fluke_identify_robot },
 { 157,  1,  0, NULL },
 { 158,  1,  0, NULL },
 { 159,  1,  0, NULL },
 { 160,  9, 11, scribbler_passthrough },   // set volume
 { 161,  9, 11, scribbler_passthrough },   // path
 { 162,  9, 11, scribbler_passthrough },   // move
 { 163,  9, 11, scribbler_passthrough },   // arc
 { 164,  9, 11, scribbler_passthrough },   // turn
 { 165,  9,  8, scribbler_passthrough },   // get position
 { 166,  9, 11, scribbler_passthrough },   // set position
 { 167,  9,  4, scribbler_passthrough },   // get angle
 { 168,  9, 11, scribbler_passthrough },   // set angle
 { 169,  9,  4, scribbler_passthrough },   // get mic env
 { 170,  9,  5, scribbler_passthrough },   // get motor stats
 { 171,  9,  8, scribbler_passthrough },   // get encoders
 { 172,  9,  1, scribbler_passthrough },   // get_ir_ex
 { 173,  9,  1, scribbler_passthrough },   // get_line_ex
 { 174,  1,  0, NULL },
 { 175,  9,  1, scribbler_passthrough },   // get_distance_ex
 { 176,  1,  0, NULL },
 { 177,  1,  0, NULL },
 { 178,  1,  0, NULL },
 { 179,  1,  0, NULL },
 { 180,  1,  0, NULL },
 { 181,  1,  0, NULL },
 { 182,  1,  0, NULL },
 { 183,  1,  0, NULL },
 { 184,  1,  0, NULL },
 { 185,  1,  0, NULL },
 { 186,  1,  0, NULL },
 { 187,  1,  0, NULL },
 { 188,  1,  0, NULL },
 { 189,  1,  0, NULL },
 { 190,  1,  0, NULL },
 { 191,  1,  0, NULL },
 { 192,  1,  0, NULL },
 { 193,  1,  0, NULL },
 { 194,  1,  0, NULL },
 { 195,  1,  0, NULL },
 { 196,  1,  0, NULL },
 { 197,  1,  0, NULL },
 { 198,  1,  0, NULL },
 { 199,  1,  0, NULL },
 { 200,  1,  0, NULL },
 { 201,  1,  0, NULL },
 { 202,  1,  0, NULL },
 { 203,  1,  0, NULL },
 { 204,  1,  0, NULL },
 { 205,  1,  0, NULL },
 { 206,  1,  0, NULL },
 { 207,  1,  0, NULL },
 { 208,  1,  0, NULL },
 { 209,  1,  0, NULL },
 { 210,  1,  0, NULL },
 { 211,  1,  0, NULL },
 { 212,  1,  0, NULL },
 { 213,  1,  0, NULL },
 { 214,  1,  0, NULL },
 { 215,  1,  0, NULL },
 { 216,  1,  0, NULL },
 { 217,  1,  0, NULL },
 { 218,  1,  0, NULL },
 { 219,  1,  0, NULL },
 { 220,  1,  0, NULL },
 { 221,  1,  0, NULL },
 { 222,  1,  0, NULL },
 { 223,  1,  0, NULL },
 { 224,  1,  0, NULL },
 { 225,  1,  0, NULL },
 { 226,  1,  0, NULL },
 { 227,  1,  0, NULL },
 { 228,  1,  0, NULL },
 { 229,  1,  0, NULL },
 { 230,  1,  0, NULL },
 { 231,  1,  0, NULL },
 { 232,  1,  0, NULL },
 { 233,  1,  0, NULL },
 { 234,  1,  0, NULL },
 { 235,  1,  0, NULL },
 { 236,  1,  0, NULL },
 { 237,  1,  0, NULL },
 { 238,  1,  0, NULL },
 { 239,  1,  0, NULL },
 { 240,  1,  0, NULL },
 { 241,  1,  0, NULL },
 { 242,  1,  0, NULL },
 { 243,  1,  0, NULL },
 { 244,  1,  0, NULL },
 { 245,  1,  0, NULL },
 { 246,  1,  0, NULL },
 { 247,  1,  0, NULL },
 { 248,  1,  0, NULL },
 { 249,  1,  0, NULL },
 { 250,  1,  0, NULL },
 { 251,  1,  0, NULL },
 { 252,  1,  0, NULL },
 { 253,  1,  0, NULL },
 { 254,  1,  0, NULL },
 { 255,  1,  0, NULL }
};
