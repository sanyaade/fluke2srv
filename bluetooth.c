// Includes some code from the remuco project by Christian Buennig

#include <bluetooth/bluetooth.h>
#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>
#include <bluetooth/rfcomm.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <syslog.h>
#include "fluke2.h"

#define SOCK_RC_SIZE (sizeof(struct sockaddr_rc))

unsigned char linkkeys_store_crc = 0;

/**
 * Register a serial port profile (SPP) service in service discovery protocol
 * (SDP) database.
 * 
 * @param port
 * The port to use for serial port (rfcomm)
 * Range: 1 <= port <= 30
 * @param name
 * Name of the service
 * @param dsc
 * Description of the service
 * @param prov
 * Provider of the service
 * @param uuid
 * UUID of the service
 * 
 * @return
 * The session used to register the service and which may be used to
 * deregister the service
 */

sdp_session_t* sdp_svc_add_spp(unsigned char port, const char *name, const char *dsc,
    const char *prov, const unsigned int uuid[])
{
 uuid_t root_uuid, l2cap_uuid, rfcomm_uuid, svc_uuid, svc_class_uuid;
 sdp_list_t *l2cap_list = 0, *rfcomm_list = 0, *root_list = 0,
    *proto_list = 0, *access_proto_list = 0, *svc_class_list = 0,
    *profile_list = 0;
 sdp_data_t *channel = 0;
 sdp_profile_desc_t profile;
 sdp_record_t record;
 sdp_session_t *session = 0;
    
 /* PART ONE */
    
 // set the general service ID
 sdp_uuid128_create( &svc_uuid, &uuid );
 memset(&record, 0, sizeof(sdp_record_t));
 sdp_set_service_id( &record, svc_uuid );
    
 // set the service class
 sdp_uuid16_create(&svc_class_uuid, SERIAL_PORT_SVCLASS_ID);
 svc_class_list = sdp_list_append(0, &svc_class_uuid);
 sdp_set_service_classes(&record, svc_class_list);
    
 // set the Bluetooth profile information
 memset(&profile, 0, sizeof(sdp_profile_desc_t));
 sdp_uuid16_create(&profile.uuid, SERIAL_PORT_PROFILE_ID);
 profile.version = 0x0100;
 profile_list = sdp_list_append(0, &profile);
 sdp_set_profile_descs(&record, profile_list);
    
 // make the service record publicly browsable
 sdp_uuid16_create(&root_uuid, PUBLIC_BROWSE_GROUP);
 root_list = sdp_list_append(0, &root_uuid);
 sdp_set_browse_groups( &record, root_list );
    
 // set l2cap information
 sdp_uuid16_create(&l2cap_uuid, L2CAP_UUID);
 l2cap_list = sdp_list_append( 0, &l2cap_uuid );
 proto_list = sdp_list_append( 0, l2cap_list );
    
 // register the RFCOMM channel for RFCOMM sockets
 sdp_uuid16_create(&rfcomm_uuid, RFCOMM_UUID);
 channel = sdp_data_alloc(SDP_UINT8, &port);
 rfcomm_list = sdp_list_append( 0, &rfcomm_uuid );
 sdp_list_append( rfcomm_list, channel );
 sdp_list_append( proto_list, rfcomm_list );
 access_proto_list = sdp_list_append( 0, proto_list );
 sdp_set_access_protos( &record, access_proto_list );
    
 // set the name, provider, and description
 sdp_set_info_attr(&record, name, prov, dsc);
    
 /* PART TWO */
    
 // connect to the local SDP server, register the service record
 session = sdp_connect( BDADDR_ANY, BDADDR_LOCAL, 0 );
 sdp_record_register(session, &record, 0);
    
 // cleanup
 sdp_data_free( channel );
 sdp_list_free( l2cap_list, 0 );
 sdp_list_free( rfcomm_list, 0 );
 sdp_list_free( root_list, 0 );
 sdp_list_free( access_proto_list, 0 );
    
 return(session);
}

/**
 * Deregister a servive previously registered with
 * sdp_register_svc_spp().
 * 
 * @param
 * the session returned by sdp_register_svc_spp() when the service
 * has been registered
 */
void sdp_svc_del(sdp_session_t *session)
{
 sdp_close(session);
}

/**
 * Set up an rfcomm server socket which is ready for accepting client
 * connections.
 * 
 * @param port (in/out parameter)
 * - if 1 <= port <= 30 then this function trys to bind to that port
 * - if port is out of that range, the first free port is used and port
 *   will be set to that number
 * @param npc
 * number of pending clients which want to connect
 * @return
 * the server socket descriptor or -1 if something failed
 */
int rfcomm_srv_sock_setup(unsigned char *port, int npc)
{
 struct sockaddr_rc addr_server;
 memset(&addr_server, 0, sizeof(struct sockaddr_rc));
 int s, ret;
    
 addr_server.rc_family = AF_BLUETOOTH;
 addr_server.rc_bdaddr = *BDADDR_ANY; // first available bt-adapter
    
 s = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
 if (s < 0) {
  flukelog("ERROR: %s socket creation failed: %s\n", __FUNCTION__, strerror(errno));
  return(-1);
 }

 ret = fcntl(s, F_SETFL, fcntl(s, F_GETFL) | O_NONBLOCK);
 if(ret < 0) {
  flukelog("ERROR: %s fcntl failed: %s\n", __FUNCTION__, strerror(errno));
  close(s);
  return(-1);
 }
    
 if (*port >= 1 && *port <= 30) {
  addr_server.rc_channel = *port;
  ret = bind(s, (struct sockaddr *)&addr_server, SOCK_RC_SIZE);
 } else {
  for (*port = 1; *port <= 30; (*port)++) {
   addr_server.rc_channel = *port;
   ret = bind(s, (struct sockaddr *) &addr_server, SOCK_RC_SIZE);
   if (ret == 0) break;
  }
 }
 if (ret < 0) {
  flukelog("ERROR: %s bind failed: %s\n", __FUNCTION__, strerror(errno));
  close(s);
  return(-1);
 }
    
 ret = listen(s, npc);
 if (ret == -1) {
  flukelog("ERROR: %s listen failed: %s\n", __FUNCTION__, strerror(errno));
  close(s);
  return(-1);
 }

 return(s);
}

/**
 * Accept a connection request on the rfcomm server socket 'ss' and store
 * connection related information in 'rec'.
 * 
 * @return
 * -1 on failue
 *  0 on success
 */
int rfcomm_srv_sock_accept(int listenfd, struct rfcomm_client *rec)
{
 socklen_t len;
 struct sockaddr_rc addr_client;
 int sockfd, ret;
    
 len = SOCK_RC_SIZE;

 sockfd = accept(listenfd, (struct sockaddr *) &addr_client, &len);
 if (sockfd < 0) {
  flukelog("ERROR: %s accept failed: %s\n", __FUNCTION__, strerror(errno));
  return(ret);
 }

 ret = fcntl(sockfd, F_SETFL, fcntl(sockfd, F_GETFL) | O_NONBLOCK);
 if(ret < 0) {
  flukelog("ERROR: %s fcntl failed: %s\n", __FUNCTION__, strerror(errno));
  close(sockfd);
  return(-1);
 }

 rec->sock = sockfd;
    
 memcpy(&rec->addr, &addr_client.rc_bdaddr, sizeof(bdaddr_t));
 ba2str((bdaddr_t*) &rec->addr, rec->addr_str);
    
 return(0);
}

// large buffer transmit
// temporarily redirect the transmit data pointer to passed buf parameter
// buf must be long lived - not on the stack

int bluetooth_send(struct rfcomm_client *btclient, char *buf, int len)
{
 int ret, cnt;

 // Attempt to send the entire message immediately
 cnt = 0;
 while(1) {
  ret = send(btclient->sock, buf + cnt, len - cnt, 0);
  // flukelog("DEBUG: %s send returned %d\n", __FUNCTION__, ret);
  if(ret == 0) break;
  else if(ret < 0) {
   if(errno == EAGAIN) break;
   else if(errno == ECONNRESET) {
    flukelog("DEBUG: %s client disconnect (send ECONNRESET)\n", __FUNCTION__);
    btclient->sock = 0;
    return(-1);
   }
   else if(errno != EINTR) {
    flukelog("ERROR: %s send error: %s\n", __FUNCTION__, strerror(errno));
    return(-1);
   }
  }
  else cnt += ret;
 }

 // If we were unable to transmit the entire message without blocking 
 // setup the background tx mechanism.  Only one background transmission 
 // at a time is allowed.  Buf must not point to stack memory.
 if(cnt < len) {
  if(!btclient->tx_ptr && !btclient->tx_len) {
   btclient->tx_ptr = buf;
   btclient->tx_len = len;
   btclient->tx_cnt = cnt;
  }
  else flukelog("ERROR: %s multiple background xmits requested\n", __FUNCTION__);
 }

 return(0);

}

// single character transmit
// fails if a bluetooth_send buffer transmit is currently active

int bluetooth_putch(struct rfcomm_client *btclient, unsigned char ch)
{

 // no background TX currently active
 if(!btclient->tx_ptr && !btclient->tx_len) {
  btclient->tx_buf[0] = ch;
  btclient->tx_ptr = btclient->tx_buf;
  btclient->tx_len = 1;
  btclient->tx_cnt = 0;
 }
 // background TX is already working on small tx_buf so add to it
 else if(btclient->tx_ptr == btclient->tx_buf) {
  btclient->tx_buf[btclient->tx_len] = ch;
  btclient->tx_len++;
 }
 // fail because background TX is live with large external buffer
 else {
  flukelog("ERROR: %s failed TX system busy\n", __FUNCTION__);
  return(-1);
 }

 return(0);

}

// medium length buffer
// len must be less than BTTXBUFSIZE
// fails if a bluetooth_send buffer transmit is currently active

int bluetooth_putstr(struct rfcomm_client *btclient, char *str, int len)
{

 // no background TX currently active
 if(!btclient->tx_ptr && !btclient->tx_len) {
  if(len > BTTXBUFSIZE) {
   flukelog("ERROR: %s len exceeds tx_buf size\n", __FUNCTION__);
   return(-1);
  }
  memcpy(btclient->tx_buf, str, len);
  btclient->tx_ptr = btclient->tx_buf;
  btclient->tx_len = len;
  btclient->tx_cnt = 0;
 }
 // background TX is already working on small tx_buf so add to it
 else if(btclient->tx_ptr == btclient->tx_buf) {
  if(btclient->tx_len + len > BTTXBUFSIZE) {
   flukelog("ERROR: %s tx_buf out of memory\n", __FUNCTION__);
   return(-1);
  }                                                               
  memcpy(btclient->tx_buf + btclient->tx_len, str, len);
  btclient->tx_len += len;
 }
 // fail because background TX is live with large external buffer
 else {
  flukelog("ERROR: %s failed TX system busy\n", __FUNCTION__);
  return(-1);
 }

 return(0);

}

int bluez_linkkeys_store()
{
 int ret, dev, fd_keys;
 FILE *fp_data;
 int retval = 0;
 struct stat filestats;
 bdaddr_t localaddr;
 unsigned char *keydata;
 char filepath[50] = "/var/lib/bluetooth/XX:XX:XX:XX:XX:XX/linkkeys";
 unsigned char buf[120];

 // Determine bluetooth address of local adapter
 dev = hci_get_route(NULL);
 if(dev < 0) {
  flukelog("ERROR: %s cannot find local bluetooth adapter\n", __FUNCTION__);
  return(-1);
 }
 hci_devba(dev, &localaddr);
 ba2str(&localaddr, filepath + 19);
 filepath[36] = '/';

 // make sure linkkeys file exists - if not maybe it was invalid and
 //   bluez deleted it.  If so zero out the fluke2 linkkeys storage.

 if(stat(filepath, &filestats)) {

  // linkkeys file does not exist
  flukelog("INFO: %s missing linkkeys file - erasing linkkeys storage\n", __FUNCTION__);

  fp_data = fopen(NONFSDATADEVICE, "wb");
  if(!fp_data) {
   flukelog("ERROR: %s error(%s) opening non-fs data device\n", __FUNCTION__, strerror(errno));
   return(-1);
  }

  memset(buf, 0, 111);
  ret = fwrite(buf, 1, 111, fp_data);
  if(ret != 111) {
   flukelog("ERROR: %s error(%s) writing zeros to non-fs data device\n", __FUNCTION__, strerror(errno));
  }

  fclose(fp_data);
  return(0);
 }

 // linkkeys file exists

 if(filestats.st_size < 50 || filestats.st_size > 10000) {
  flukelog("ERROR: %s linkkeys file size (%d) is wrong\n", __FUNCTION__, filestats.st_size);
  return(-1);
 }

 fd_keys = open(filepath, O_RDONLY);
 if(fd_keys < 0) {
  flukelog("ERROR: %s error(%s) opening linkkeys file(%s)\n", __FUNCTION__, strerror(errno), filepath);
  return(-1);
 }

 keydata = mmap(NULL, filestats.st_size, PROT_READ, MAP_SHARED, fd_keys, 0);
 if(keydata == MAP_FAILED) {
  flukelog("ERROR: %s error(%s) mmap linkkeys file\n", __FUNCTION__, strerror(errno));
  close(fd_keys);
  return(-1);
 }

 buf[0] = filestats.st_size & 0xFF;
 buf[1] = (filestats.st_size >> 8) & 0xFF;
 buf[2] = crc8calc(keydata, filestats.st_size);

 // just return if the linkkeys file has not changed since the last store
 if(buf[2] == linkkeys_store_crc) {
  goto linkkeys_store_fail;
 }

 linkkeys_store_crc = buf[2];

 fp_data = fopen(NONFSDATADEVICE, "wb");
 if(!fp_data) {
  flukelog("ERROR: %s error(%s) opening non-fs data device\n", __FUNCTION__, strerror(errno));
  retval = -1;
  goto linkkeys_store_fail;
 }

 ret = fwrite(buf, 1, 3, fp_data);
 if(ret != 3) {
  flukelog("ERROR: %s error(%s) writing to non-fs data device\n", __FUNCTION__, strerror(errno));
 }
 ret = fwrite(keydata, 1, filestats.st_size, fp_data);
 if(ret != filestats.st_size) {
  flukelog("ERROR: %s error(%s) writing to non-fs data device\n", __FUNCTION__, strerror(errno));
 }

 fclose(fp_data);

linkkeys_store_fail:

 if(munmap(keydata, filestats.st_size)) {
  flukelog("ERROR: %s error(%s) munmap linkkeys file\n", __FUNCTION__, strerror(errno));
  close(fd_keys);
  return(-1);
 }

 close(fd_keys);

 return(retval);
}

// This function must be called before bluetoothd starts for it to work.
//  Not sure why but that means it must run in the rc.d startup scripts 
//  before fluke2srv is started.  The bluekeyload executable just 
//  calls this function.
//
int bluez_linkkeys_load()
{
 int ret, dev, fd_data;
 FILE *fp_keys;
 unsigned short datasize;
 int retval = 0;
 struct stat filestats;
 bdaddr_t localaddr;
 unsigned char *keydata;
 unsigned char keycrc;
 char filepath[50] = "/var/lib/bluetooth/XX:XX:XX:XX:XX:XX/linkkeys";

 openlog("bluekeyload", LOG_CONS, LOG_USER);

 // Determine bluetooth address of local adapter
 dev = hci_get_route(NULL);
 if(dev < 0) {
  syslog(LOG_USER, "ERROR: %s cannot find local bluetooth adapter\n", __FUNCTION__);
  return(-1);
 }
 hci_devba(dev, &localaddr);
 ba2str(&localaddr, filepath + 19);

 // check if adapter directory exists and create it if necessary
 if(stat(filepath, &filestats)) {
  syslog(LOG_USER, "INFO: %s creating %s directory\n", __FUNCTION__, filepath);
  if(mkdir(filepath, 0755)) {
   syslog(LOG_USER, "ERROR: %s error(%s) creating directory %s\n", __FUNCTION__, strerror(errno), filepath);
   return(-1);
  }
 }

 filepath[36] = '/';

 fd_data = open(NONFSDATADEVICE, O_RDONLY);
 if(fd_data < 0) {
  syslog(LOG_USER, "ERROR: %s error(%s) opening non-fs data device\n", __FUNCTION__, strerror(errno));
  return(-1);
 }

 ret = read(fd_data, &datasize, 2);
 if(ret != 2) {
  syslog(LOG_USER, "ERROR: %s error(%s) reading from non-fs data device\n", __FUNCTION__, strerror(errno));
  close(fd_data);
  return(-1);
 }

 if(datasize < 50 || datasize > 10000) {
  if(datasize != 0) syslog(LOG_USER, "ERROR: %s stored linkkeys size (%d) is wrong\n", __FUNCTION__, datasize);
  close(fd_data);
  return(-1);
 }

 keydata = mmap(NULL, datasize, PROT_READ, MAP_SHARED, fd_data, 0);
 if(keydata == MAP_FAILED) {
  syslog(LOG_USER, "ERROR: %s error(%s) mmap data device\n", __FUNCTION__, strerror(errno));
  close(fd_data);
  return(-1);
 }

 keycrc = crc8calc(keydata+3, datasize);
 if(keycrc != keydata[2]) {
  syslog(LOG_USER, "ERROR: %s CRC mismatch on linkkeys data\n", __FUNCTION__);
  retval = -1;
  goto linkkeys_load_fail;
 }

 fp_keys = fopen(filepath, "wb");
 if(!fp_keys) {
  syslog(LOG_USER, "ERROR: %s error(%s) opening linkkeys file(%s)\n", __FUNCTION__, strerror(errno), filepath);
  retval = -1;
  goto linkkeys_load_fail;
 }

 ret = fwrite(keydata+3, 1, datasize, fp_keys);
 if(ret != datasize) {
  syslog(LOG_USER, "ERROR: %s error(%s) writing to linkkeys file\n", __FUNCTION__, strerror(errno));
 }

 fclose(fp_keys);

linkkeys_load_fail:

 if(munmap(keydata, datasize)) {
  syslog(LOG_USER, "ERROR: %s error(%s) munmap data device\n", __FUNCTION__, strerror(errno));
  close(fd_data);
  return(-1);
 }

 close(fd_data);

 return(retval);
}

int bluez_set_discoverable()
{
 struct hci_dev_req dr;
 struct hci_dev_info di;
 int dev, sock;

 // When the bluetooth discoverable flags are set during system startup the
 //   kernel sometimes will immediately disable them.

 // Determine bluetooth address of local adapter
 dev = hci_get_route(NULL);
 if(dev < 0) {
  flukelog("ERROR: %s cannot find local bluetooth adapter\n", __FUNCTION__);
  return(-1);
 }

 if(hci_devinfo(dev, &di) < 0) {
  flukelog("ERROR: %s error(%s) requesting device info\n", __FUNCTION__, strerror(errno));
  return(-1);
 }

 // if we are already discoverable then just return
 if(hci_test_bit(HCI_PSCAN, &di.flags) && hci_test_bit(HCI_ISCAN, &di.flags))
  return(0);

 // set discoverable flag

 sock = hci_open_dev(dev);
 if(sock < 0) {
  flukelog("ERROR: %s error(%s) opening bluetooth device\n", __FUNCTION__, strerror(errno));
  return(-1);
 }

 dr.dev_id  = dev;
 dr.dev_opt = SCAN_PAGE | SCAN_INQUIRY;

 if(ioctl(sock, HCISETSCAN, (unsigned long) &dr) < 0) {
  flukelog("ERROR: %s error(%s) on ioctl enabling scan mode\n", __FUNCTION__, strerror(errno));
  return(-1);
 }

 close(sock);

 return(0);
}
