# cross compiler
CC = /home/danielbw/arm/cs/glibc/arm-2011.09/bin/arm-none-linux-gnueabi-gcc
# local compile on fluke
# CC = gcc   
CFLAGS  = -O2
# only necessary for cross compile
INCLUDES = -Iarm-include
# only necessary for cross compile
LFLAGS = -Larm-lib
LIBS = -lbluetooth -ljpeg

all: fluke2srv bluekeyload

fluke2srv:		main.o fluke2.o fluke2cmd.o camera.o bluetooth.o gpio.o misc.o scribbler.o
		$(CC) $(CFLAGS) $(INCLUDES) -o fluke2srv main.o fluke2.o fluke2cmd.o camera.o bluetooth.o gpio.o misc.o scribbler.o $(LFLAGS) $(LIBS)

bluekeyload: bluekeyload.o bluetooth.o misc.o
		$(CC) $(CFLAGS) $(INCLUDES) -o bluekeyload bluekeyload.o bluetooth.o misc.o $(LFLAGS) $(LIBS)

main.o:		main.c fluke2.h
		$(CC) $(CFLAGS) $(INCLUDES) -c main.c
fluke2.o:		fluke2.c fluke2.h
		$(CC) $(CFLAGS) $(INCLUDES) -c fluke2.c
fluke2cmd.o:		fluke2cmd.c fluke2.h
		$(CC) $(CFLAGS) $(INCLUDES) -c fluke2cmd.c
camera.o:		camera.c fluke2.h
		$(CC) $(CFLAGS) $(INCLUDES) -c camera.c
bluetooth.o:		bluetooth.c fluke2.h
		$(CC) $(CFLAGS) $(INCLUDES) -c bluetooth.c
gpio.o:		gpio.c fluke2.h
		$(CC) $(CFLAGS) $(INCLUDES) -c gpio.c
misc.o:		misc.c fluke2.h
		$(CC) $(CFLAGS) $(INCLUDES) -c misc.c
scribbler.o:		scribbler.c fluke2.h
		$(CC) $(CFLAGS) $(INCLUDES) -c scribbler.c
bluekeyload.o:		bluekeyload.c fluke2.h
		$(CC) $(CFLAGS) $(INCLUDES) -c bluekeyload.c

clean:
		rm -f fluke2srv bluekeyload *.o
