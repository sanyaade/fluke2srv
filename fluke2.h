#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>

#define DEBUGPRINTCONSOLE 1

#define FLUKE2_VERSION "fluke:3.0.9"

#define SCRIBBLERTTY       "/dev/ttyfiq"
#define CAMERADEVICE       "/dev/spidev0.0"
#define NONFSDATADEVICE    "/dev/mmcblk0p3"
#define NONFSDATA_LEGACYFLASH_OFFSET  16384

#define SCRIBCMD_GET_INFO               80
#define SCRIBCMD_SET_ECHO_MODE          98
#define SCRIBCMD_SET_MOTORS             109
#define FLUKECMD_GET_JPEG_GREY_HEADER   135
#define FLUKECMD_GET_JPEG_COLOR_HEADER  137

#define GPIOOUTPUT         1
#define GPIOINPUT          0

#define GPIO_SERVO1        1
#define GPIO_SERVO2        0
#define GPIO_SERVO3        2
#define GPIO_SERVO4        3
#define GPIO_FRONTLED      5     // used by kernel for load display
#define GPIO_IROUT         6
#define GPIO_IRIN          7
#define GPIO_SCRIB_RX      8     // used by kernel for scribbler tty driver
#define GPIO_SCRIB_TX      9     // used by kernel for scribbler tty driver
#define GPIO_SCRIB_RST     10    
#define GPIO_CAM_SCL       11
#define GPIO_CAM_SDA       12
#define GPIO_PICINIT       13    // used by kernel for SPI driver
#define GPIO_BRIGHT_LED    14
#define GPIO_PICSIZE       86

#define BRIGHT_LED_DEFAULT_PWM  0x0B50

#define BTRXBUFSIZE       (128 * 1024)
#define BTTXBUFSIZE       4096
#define ERRORLOGSIZE      10000

#define IMAGE_EXPIRE_SECONDS  1
#define JPEG_QUALITY_GOOD     80
#define JPEG_QUALITY_BAD      40
#define FIXEDSHIFT            8
#define NUM_WINDOWS           4
#define RLEBUFSIZE            0xFFFF
#define CAMERA_DEF_GAIN       75    //78    // 0x3A

#define CALLER_MAIN           10
#define CALLER_MAIN_REPEAT    11
#define CALLER_CAMERA         12
#define CALLER_SERIAL         13

#define PROP_SHUTDOWN_CMD            0
#define PROP_DOWNLOAD_RUN_BINARY     1
#define PROP_DOWNLOAD_EEPROM         2
#define PROP_DOWNLOAD_RUN_EEPROM     3
#define PROP_DOWNLOAD_SHUTDOWN       4

#define msleep(msec) usleep(msec*1000);

struct rfcomm_client {
 int sock;
 char *rx_buf;
 char *rx_data;
 int rx_len;
 int rx_end;
 int rx_pos;
 char *tx_buf;
 char *tx_ptr;
 int tx_len;
 int tx_cnt;
 unsigned char command;
 unsigned char lastcommand;
 int caller;
 char addr_str[19];
 struct btaddr {
  unsigned char b[6];
 } addr;
};

struct fluke_command {
 unsigned char cmd;
 unsigned char cmdlen;           // length of command + command data
 unsigned char responselen;      // only used for scribbler commands
 int (*process)(struct rfcomm_client *);
};

struct error_record {
 char *header;
 char *log;
 int pos;
 int size;
};

struct camera_control {
 int fd;
 unsigned char command;
 struct rfcomm_client *client;
 int jpegsize;
 int jpegquality;
 int colorflag;
 int dojpeg;
 int imagewidth;
 int imageheight;
 char *image;
 char *jpeg;
 char *jpegmyro;
};

struct serial_control {
 int fd;
 unsigned char command;
 struct rfcomm_client *client;
 char rx_buf[100];
 char tx_buf[100];
 int rx_len;
 int tx_len;
 int rx_cnt;
 int scribbler_version;
};

struct servo_control {
 int fd;
 int open;
 int resume;
 unsigned char pos;
};

struct camera_window {
 int xlow;
 int xhigh;
 int ylow;
 int yhigh;
 int xstep;
 int ystep;
};

struct colordef {
 unsigned char y_low;
 unsigned char y_high;
 unsigned char u_low;
 unsigned char u_high;
 unsigned char v_low;
 unsigned char v_high;
};

// functions in fluke2.c
int scribbler_passthrough(struct rfcomm_client *btclient);
int fluke_get_errors(struct rfcomm_client *btclient);
int fluke_set_picsize(struct rfcomm_client *btclient);
int fluke_servo(struct rfcomm_client *btclient);
int fluke_enable_pan(struct rfcomm_client *btclient);
int fluke_update_firmware(struct rfcomm_client *btclient);
int fluke_save_eeprom(struct rfcomm_client *btclient);
int fluke_restore_eeprom(struct rfcomm_client *btclient);
int fluke_reset(struct rfcomm_client *btclient);
int fluke_get_rle(struct rfcomm_client *btclient);
int fluke_get_image(struct rfcomm_client *btclient);
int fluke_get_image_window(struct rfcomm_client *btclient);
int fluke_get_ir_left(struct rfcomm_client *btclient);
int fluke_get_ir_center(struct rfcomm_client *btclient);
int fluke_get_ir_right(struct rfcomm_client *btclient);
int fluke_get_image_window_sum(struct rfcomm_client *btclient);
int fluke_get_battery(struct rfcomm_client *btclient);
int fluke_get_serial_mem(struct rfcomm_client *btclient);
int fluke_get_scrib_program(struct rfcomm_client *btclient);
int fluke_get_camera_param(struct rfcomm_client *btclient);
int fluke_get_blob_window(struct rfcomm_client *btclient);
int fluke_get_blob(struct rfcomm_client *btclient);
int fluke_set_led_on(struct rfcomm_client *btclient);
int fluke_set_led_off(struct rfcomm_client *btclient);
int fluke_set_rle(struct rfcomm_client *btclient);
int fluke_set_ir_power(struct rfcomm_client *btclient);
int fluke_set_serial_mem(struct rfcomm_client *btclient);
int fluke_set_scrib_program(struct rfcomm_client *btclient);
int fluke_set_scrib2_program(struct rfcomm_client *btclient);
int fluke_reset_scrib2(struct rfcomm_client *btclient);
int fluke_set_scrib_program_batch(struct rfcomm_client *btclient);
int fluke_identify_robot(struct rfcomm_client *btclient);
int fluke_set_start_program(struct rfcomm_client *btclient);
int fluke_reset_scribbler(struct rfcomm_client *btclient);
int fluke_serial_erase(struct rfcomm_client *btclient);
int fluke_set_bright_led(struct rfcomm_client *btclient);
int fluke_set_window(struct rfcomm_client *btclient);
int fluke_set_forwardness(struct rfcomm_client *btclient);
int fluke_white_balance_on(struct rfcomm_client *btclient);
int fluke_white_balance_off(struct rfcomm_client *btclient);
int fluke_set_camera_param(struct rfcomm_client *btclient);
int fluke_set_uart(struct rfcomm_client *btclient);
int fluke_pass_byte(struct rfcomm_client *btclient);
int fluke_set_passthrough(struct rfcomm_client *btclient);
int fluke_get_jpeg_grey_header(struct rfcomm_client *btclient);
int fluke_get_jpeg_grey_scan(struct rfcomm_client *btclient);
int fluke_get_jpeg_color_header(struct rfcomm_client *btclient);
int fluke_get_jpeg_color_scan(struct rfcomm_client *btclient);
int fluke_set_pass_n_bytes(struct rfcomm_client *btclient);
int fluke_get_pass_n_bytes(struct rfcomm_client *btclient);
int fluke_get_pass_bytes_until(struct rfcomm_client *btclient);
int fluke_get_version(struct rfcomm_client *btclient);
int fluke_set_passthrough_on(struct rfcomm_client *btclient);
int fluke_set_passthrough_off(struct rfcomm_client *btclient);
int fluke_get_ir_message(struct rfcomm_client *btclient);
int fluke_send_ir_message(struct rfcomm_client *btclient);
int fluke_set_ir_emitters(struct rfcomm_client *btclient);

// functions in misc.c
void flukelog(char *fmt, ...);
size_t strlcpy(char *dest, const char *src, size_t size);
size_t strlcat(char *dest, const char *src, size_t count);
int read_a2d(char channel, int *val, char *valstr);
int set_pwm(int val);
void udelay(int microsec_delay);
unsigned char crc8calc(unsigned char *buf, unsigned int len);
int flashdata_write(char *buf, int offset, int len);
int flashdata_read(char *buf, int offset, int len);
int set_servo(unsigned char srv_id, unsigned char srv_pos);
int pause_servo();
int resume_servo();

// functions in bluetooth.c
sdp_session_t* sdp_svc_add_spp(unsigned char port, const char *name, const char *dsc,
    const char *prov, const unsigned int uuid[]);
void sdp_svc_del(sdp_session_t *session);
int rfcomm_srv_sock_setup(unsigned char *port, int npc);
int rfcomm_srv_sock_accept(int ss, struct rfcomm_client *rec);
int bluetooth_send(struct rfcomm_client *btclient, char *buf, int len);
int bluetooth_putch(struct rfcomm_client *btclient, unsigned char ch);
int bluetooth_putstr(struct rfcomm_client *btclient, char *str, int len);
int bluez_linkkeys_store();
int bluez_linkkeys_load();

// functions in gpio.c
int gpio_export(unsigned int gpio);
int gpio_unexport(unsigned int gpio);
int gpio_set_dir(unsigned int gpio, unsigned int out_flag);
int gpio_set_value(unsigned int gpio, unsigned int value);
int gpio_get_value(unsigned int gpio, unsigned int *value);
int gpio_set_edge(unsigned int gpio, char *edge);
int gpio_fd_open(unsigned int gpio);
int gpio_fd_close(int fd);

// functions defined in camera.c
int camera_i2c_write(unsigned int address, unsigned int data);
int camera_i2c_read(unsigned int address, unsigned char *data);
int camera_set_gain(int gain);
int camera_start_grab_image();
int camera_image_compress(char *cambuf, char *jpegbuf, int *jpegsize, int jpegquality, int colorflag);
void bayer_demosaic(unsigned char *rgb, unsigned char *bayer, int width, int height);
void image_rgb2gray(unsigned char *rgb);
void image_rgb2yuv(unsigned char *rgb);
void image_rgb2vyuy(unsigned char *rgb);
void yuv2rgb(unsigned char *r, unsigned char *g, unsigned char *b,
             unsigned char y, unsigned char u, unsigned char v);

// functions defined in scribbler.c
int prop_serial_init(const char* port);
void prop_serial_done(void);
void prop_hwreset(void);
int prop_lfsr_iterate(uint8_t *lfsr);
int prop_getBit(unsigned char *data);
int prop_sendlong(uint32_t data);
int prop_hwfind(int *version);
int prop_upload(const uint8_t* dlbuf, int count, int type);
